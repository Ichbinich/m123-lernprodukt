# M123 - Hands-on Lernprodukt eines Lernenden

## Ausgangslage
Dieses Repository wurde vom Lernenden **Hoang Nguyen** erstellt und dient als Beispiel für die **Praxisarbeiten** im **Modul 123**




# Inhaltsverzeichnis
 - [Praktische Arbeit DHCP](01_DHCP/README.md)
 - [Praktische Arbeit DNS](02_DNS/README.md)
 - [Praktische Arbeit Samba-Fileshare](03.1_FileShare/README.md)
 - [Praktische Arbeit Cisco HandsOn](03.2_Cisco/README.md)
 - [Praktische Arbeit Filius Grossrepetition](03.3_RepetitionFilius/README.md)


