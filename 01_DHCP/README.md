# DHCP-Konfigurationsdokumentation

Author: **Hoang Thang Nguyen** <br>
Updated Date: **10.01.2023**

# Inhaltsverzeichnis

- [DHCP-Konfigurationsdokumentation](#dhcp-konfigurationsdokumentation)
- [Inhaltsverzeichnis](#inhaltsverzeichnis)
- [1. Einführung \& Erklärung](#1-einführung--erklärung)
  - [1.1. Einführung](#11-einführung)
  - [1.2. DHCP](#12-dhcp)
- [2. Logik, Übersicht und Denkweise](#2-logik-übersicht-und-denkweise)
- [3. Netzwerkadapter](#3-netzwerkadapter)
- [4. Installation Ubuntu Server](#4-installation-ubuntu-server)
  - [4.1. Vorkonfiguration des Servers](#41-vorkonfiguration-des-servers)
  - [4.2. Installation des Servers via Terminal](#42-installation-des-servers-via-terminal)
- [5. Client-Konfiguration](#5-client-konfiguration)
- [6. Qualified Troubleshooting](#6-qualified-troubleshooting)
  - [6.1. First Troubleshoot](#61-first-troubleshoot)
  - [6.2. Second Troubleshoot](#62-second-troubleshoot)

# 1. Einführung & Erklärung
## 1.1. Einführung

Die Aufgabe ist, einen Ubuntu-Server aufzusetzen und darauf eine DHCP-Anwendung via Command Prompt (Terminal) zu installieren. Mit der DHCP-Anwendung sollte der DORA-Prozess mit einem Client funktionieren.<bd>
Die einzelnen Schritte werden grob in dieser Dokumentation beschrieben.

## 1.2. DHCP

Ein DHCP-Dienst wird benutzt, um die Vergabe von IP-Adressen zu automatisieren und konkret zu vereinfachen. Bei einer Anzahl von 10 Benutzern ist die Verwendung eines DHCP-Servers sinnvoll und empfohlen.
Die Verbindungsschritte und Kommunikation zwischen DHCP-Server und Client sind im DORA-Prozess beschrieben. Es gibt 4 wichtige Schritte:
**DHCPDiscover** – Genaues Suchen der einzelnen Netzwerkkomponenten per Broadcast.
**DHCPOffer** – DHCP erhält per Port 67 eine Anfrage zur Vergabe und Reservierung einer IP-Adresse und schickt die einzelnen Daten (Lease-Time, IP, Gateway, DNS) zur Verbindung an dem Client zurück (via Broadcast).
**DHCPRequest** – Der Client akzeptiert die Offerte und meldet es per Broadcast am DHCP-Server.
**DHCPAck(nowledge)** – Der DHCP bekommt die akzeptierte Offerte zurück und fängt mit der Reservierung der IP-Adresse an, vielleicht auch schon die direkte Vergabe der IP-Adresse.

# 2. Logik, Übersicht und Denkweise

<img src="../04_Reliquum/GitLabOpus/1.png" alt="" width="300"/>

Fangen wir mit dem DHCP-Server an. Er hat zwei Netzwerkadapter, eines für das interne Netzwerk mit dem DHCP-Client, in diesem Fall dem DALLUC-Client und eines mit dem er Datenpakete via «sudo apt» Pakete holen kann.

Der Netzwerkadapter für das **interne** Netzwerk heisst **Ens33** und besitzt die Subnetzadresse 192.168.100.0. Über diesen Kanal spielt der DORA-Prozess zwischen Client und Server ab.
Der Netzwerkadapter für das **äussere** Netzwerk heisst **Ens37** und ist der Kanal für das Holen der Datenpaketen. Die Subnetzadresse hier lautet 192.168.98.0.

Der Range des IP-Pools überstimmt den Vorgaben des Sota Handel GmbH. Der IP-Pool liegt zwischen 192.168.100.151 – 192.168.100.200. Alle IP-Adressen der Sota Handel GmbH liegen im gleichen Subnetz (es gibt nur ein Subnetz). 
Der DHCP-Server selbst nimmt die erste Adresse 192.168.100.151.
Unser Tor des Netzwerks lautet 192.168.100.1, ab hier wird mit Ens33 kommuniziert.

# 3. Netzwerkadapter

Bevor wir mit dem Hauptthema anfangen, müssen wir die Netzwerkadapter erstellen und konfigurieren. Die beiden Netzwerkadapter stellen zwei Netzwerke dar, eines für das interne und das für das Holen der Datenpakete für den DHCP-Server. <br>
<img src="../04_Reliquum/GitLabOpus/2.png" alt="" width="300"/>

VMnet0 spielt in unserem Fall keine Rolle. Wir schauen nur die beiden Unteren an. Wie schon oben beschrieben hat ein Netzwerkadapter die Adresse 192.168.98.0 und die andere 192.168.100.0.  VMnet9 ist unser Kanal für das Holen der Datenpakete. M117_LAB ist das private Netzwerk, in dem unser DHCP-Server und Client liegen.

- **1**.	Mit «Add Network» ein neues Netzwerkadapter hinzufügen.

- **2**.	Bei M117_LAB unbedingt «Connect a host virtual adapter to this network» und «Use local DHCP service to distribute IP address to VMs» ausschalten, damit der DHCP von VMware nicht in unser Netzwerk reinfunkt. Noch die Subnetz-IP-Adresse eingeben. <br>
  ➜ <img src="../04_Reliquum/GitLabOpus/3.png" alt="" width="300"/>

- **3**. Nochmal ein Netzwerkadapter hinzufügen und bei den Optionen NAT wählen und die zwei Optionen, die oben beschrieben worden sind, wählen. <br>
  ➜ <img src="../04_Reliquum/GitLabOpus/4.png" alt="" width="300"/>



# 4. Installation Ubuntu Server
## 4.1. Vorkonfiguration des Servers

Bevor wir mit dem DHCP-Server anfangen, müssen wir ja noch das Betriebsystem aufsetzen. Mit den folgenden Schritten habe ich das gemacht.

- **1**.	Das ist unser Startbildschirm, hier habe ich «try or install ubuntu server» ausgewählt. <br>
  ➜ <img src="../04_Reliquum/GitLabOpus/5.png" alt="" width="300"/>

- **2**. Die folgenden Schritte nach dem Wählen der Option sind einfach nachzuvollziehen. Man soll nur durchklicken und Einstellungen nach Belieben auswählen.

- **3**. Die Storage Configuration soll so aussehen. <br>
  ➜ <img src="../04_Reliquum/GitLabOpus/6.png" alt="" width="300"/>

- **4**. Das Eingeben der Namen ist Ihnen überlassen. <br>
  ➜ <img src="../04_Reliquum/GitLabOpus/7.png" alt="" width="300"/>

- **5**. Nach einigen weiteren Schritten muss man das Betriebssystem installieren. So soll es dann aussehen. Ab hier muss man ein bisschen Geduld haben, während das Betriebssystem heruntergeladen wird und konfiguriert wird. <br>
  ➜ <img src="../04_Reliquum/GitLabOpus/8.png" alt="" width="300"/>  

- **6**. Nach ein paar Minuten poppt das Terminal auf, wo wir unsere Befehle eintippen. <br>
  ➜ <img src="../04_Reliquum/GitLabOpus/9.png" alt="" width="300"/>  

## 4.2. Installation des Servers via Terminal

Alle Befehle werden mit Root ausgeführt. Der Root-User ist das Äquivalent zum Administrator von Windows mit allen Ausführ- und Bearbeitungsrechten. Mit sudo su kann man in den Root-Modus wechseln. Man muss aber nicht, man kann weiterhin sudo bei jedem Befehl eingeben. 
<br>
Jetzt folgen weitere Installationsschritte zum Herunterladen und Aufsetzen unseres ISC-DHCP-Servers.

- **1**. Mit folgendem Befehl laden wir unser DHCP-Dienst herunter. <br>
  ```sudo apt install isc-dhcp-server```

- **2**. Nach einer Weile soll der DHCP verfügbar auf dem System heruntergeladen sein. Funktionieren soll er aber noch nicht, weil er noch nicht konfiguriert worden ist. Schauen wir doch nach, ob er läuft. Mit dem Befehl unterhalb der Nachricht überprüft man den Status des Dienstes. <br>
  ```sudo systemctl status isc-dhcp-server``` <br>
  ➜ <img src="../04_Reliquum/GitLabOpus/10.png" alt="" width="300"/>

- **3**. Wir wollen jetzt also den DHCP-Server konfigurieren. Zuerst mal ein Backup der Config-Datei. Dies macht man mit: 
  ```sudo cp /etc/dhcp/dhcpd.conf  /etc/dhcp/dhcpd.conf.backup```

- **4**. Mit ```sudo nano /etc/dhcp/dhcpd.conf``` öffnet man diese zuvor redundant gespeicherte Datei. Das Bild unten ist die geöffnete Konfigurationsdatei, wo wir unsere IP-Adressen, Pools und sonstiges konfigurieren. <br>
  ➜ <img src="../04_Reliquum/GitLabOpus/11.png" alt="" width="300"/>

- **5**. Geben wir doch unsere Adressen doch an. Ein bisschen herunterscrollen und die Stelle im Bild suchen. Die Subnetz-IP sind 192.168.100.0 und Netzmaske 255.255.255.0. Unser IP-Pool (range) ist 192.168.100.151 - 192.168.100.200. Wir geben für den DNS-Server irgendetwas an. Auch für den Domain-Name geben wir etwas an. Unterhalb der Domain-Name befindet sich der Gateway, in unserem Fall 192.168.100.1. Die Broadcast-Adresse und die Leasetime in Minuten werden ebenfalls eingegeben. Schon ist unsere Konfiguration für den DHCP-Server teils fertig. <br>
  ➜ <img src="../04_Reliquum/GitLabOpus/12.png" alt="" width="300"/>

- **6**. Um jetzt zu bestimmen, dass Ens37, also unser Netzwerkadapter für unser internes Netzwerk als Kanal für den DORA-Prozess wird, müssen wir das ebenfalls konfigurieren. Ich werde die Interface-Datei öffnen. <br>
  ```sudo nano /etc/default/isc-dhcp-server```

- **7**. Bei INTERFACEv4 soll anstatt **eth’0 -> Ens37** stehen. Wir geben vor, dass Ens37 das Netzwerkadapter für die DHCP-Requests ist. <br>
  ➜ <img src="../04_Reliquum/GitLabOpus/13.png" alt="" width="300"/>

- **8**. Dieser Schritt wurde von mir vergessen, holen wir das noch nach. Auf der Menüleiste unter VM und Settings fügen wir den Netzwerkadapter Ens33 hin, um zu überprüfen, ob die VM überhaupt Datenpakete ziehen kann. Man kann auch schon Ens37 hinzufügen, wenn man das Überprüfen überspringen möchte. Zwei Netzwerkkarten können im Ubuntu für genug Stress sorgen. <br>
  ➜ <img src="../04_Reliquum/GitLabOpus/14.png" alt="" width="300"/> <br>
  ➜ <img src="../04_Reliquum/GitLabOpus/15.png" alt="" width="300"/> <br>
  ➜ <img src="../04_Reliquum/GitLabOpus/16.png" alt="" width="300"/> <br>

- **9**. Beim Installieren des Betriebssystems hatte ich das Hinzufügen der IP-Adressen zu den beiden Netzwerkadaptern völligsten vergessen. Holen wir auch das nach. <br>
  ```sudo nano /etc/netplan/00-installer-config.yaml``` <br>
  ➜ <img src="../04_Reliquum/GitLabOpus/17.png" alt="" width="300"/> <br>
  Bei **Ens37** geben wir das obere Adressenfeld ein. Wir bestimmen hier, dass unser Server eine statische IP-Adresse bekommt und nicht per DHCP. Die DNS geben wir als 8.8.8.8 und 8.8.4.4 an. Unser Gateway für den DHCP-Server ist 192.168.100.1
  <br>
  Für **Ens33** geben wir nur an, dass die IP-Adresse über DHCP bezogen wird. Das war auch schon alles. Jetzt haben beide Netzwerkadapter ihre IP-Adressen, überprüfen wir noch ganz kurz mit **ip a** und **ip r**. Mit ip a werden alle IP-Adressen aufgezeigt, mit ip r die Gateways. <br>
  ➜ <img src="../04_Reliquum/GitLabOpus/18.png" alt="" width="300"/> <br>
  ➜ <img src="../04_Reliquum/GitLabOpus/19.png" alt="" width="300"/> <br>

- **10**. Zu den letzten Schritten gehört das Rebooten unseres DHCP-Servers. Das geschieht mit ```sudo systemctl restart isc-dhcp-server```. Mit ```sudo systemctl status isc-dhcp-server``` den Status des DHCP-Servers überprüfen. Im Bild ist das Endresultat.  Alles läuft wie geplant. <br>
  ➜ <img src="../04_Reliquum/GitLabOpus/20.png" alt="" width="300"/> <br>

  **➜ Unser DHCP-Server funktioniert jetzt, wir schauen den Clientteil an.**

# 5. Client-Konfiguration

Zuerst binden wir den Netzwerkadapter Ens33 / M117_LAB ein, damit unser DHCP-Server mit dem Client kommunizieren kann. Hier gehen wir wieder auf VM -> Settings und wählen beim Network Adapter 2 **M117_LAB** aus. <br>

Ich gehe konfiguriere den PC von DALLUC. Zuerst in den Windows-Einstellungen gehen und für «Netzwerk und Internet» suchen und dann «Change adapter options» auswählen. <br>
<img src="../04_Reliquum/GitLabOpus/21.png" alt="" width="300"/> <br>

Wähle Ethernet1 aus. <br>
<img src="../04_Reliquum/GitLabOpus/22.png" alt="" width="300"/> <br>

Gehe dann auf Properties und suche für <img src="../04_Reliquum/GitLabOpus/23.png" alt="" width="260"/> und klicke darauf. <br>

«IP-Adresse automatisch beziehen / Obtain an IP adresse automatically» auswählen. <br> 
<img src="../04_Reliquum/GitLabOpus/24.png" alt="" width="260"/> <br>
Jetzt haben wir alles schön konfiguriert.


Wir öffnen das Command Prompt und geben ipconfig /release ein, um die aktuelle IP-Adresse freizugeben und geben ipconfig /renew ein, um eine neue zu holen. Am Schluss soll der Client vom DHCP-Server eine IP-Adresse in der IP-Range bekommen, Internet bekommt er aber noch nicht. <br>
<img src="../04_Reliquum/GitLabOpus/25.png" alt="" width="260"/> <br>

# 6. Qualified Troubleshooting

Bei der Installation kam ich auf diverse Probleme, die ich nicht lösen konnte. Erstens lief der DHCP-Server nicht rund und gab mir immer wieder eine Fehlermeldung zurück, was mir nach einiger Zeit langsam Sorgen machte.

## 6.1. First Troubleshoot

  -	Ich öffnete die Konfigurationsdatei des DHCP-Servers und konnte dort nichts Relevantes auffinden.
  -	Ich öffnete die 00-Installer-config-Datei und gab dort meine statische IP-Adresse sowie dynamische IP-Adresse ein. Das half mir, endlich dem zweiten Netzwerkadapter eine IP-Adresse zu vergeben, da seit Anfang bei dem zweiten Netzwerkadapter keine IP-Adresse vorhanden war.
  -	Ausserdem vergass ich die Interfaces, also Kanäle für die DHCP-Requests zu bestimmen. Bei der Interface-Datei gab ich den richtigen Netzwerkadapter ein und versuchte den DHCP-Server anzuschalten, was auch geschah und zwar erfolgreich. Mein DHCP funktioniert wieder.

## 6.2. Second Troubleshoot

  -	Mein zweiter Netzwerkadapter bekam keine IP-Adresse, auch das machte mir nach einiger Zeit sehr grosse Sorgen, denn ohne IP-Adresse kann dieser Kanal nicht für das Holen der Datenpakete benutzt werden.
  -	Ich ging in die 00-Installer-config-Datei und fügte für den zweiten Netzwerkadapter eine dynamische vom DHCP-Server der VMWare IP-Adresse hinzu. Das ging irgendwie auch nicht rund, die IP-Adresse war beim eintippen von «ip a» vorhanden, das Herunterladen von Datenpaketen ging aber trotzdem nicht.
  -	Ich löschte den Netzwerkadapter, fügte einen neuen hinzu und gab ihm eine neue IP-Adresse.
  -	Schlussendlich gelang es mir Datenpakete sowie Updates zu holen und diese zu upgraden.

**1.**	IP-Adresse überprüfen <br>
**2.**	Stand ders DHCP-Servers überprüfen <br>
**3.**  Sämtliche Konfigurationsdateien für Fehler überprüfen <br>
**4.**	Netzwerkadapter bearbeiten oder auch löschen und neu hinzufügen. <br>
