# FS-Konfigurationsdokumentation

Author: **Hoang Thang Nguyen** <br>
Updated Date: **14.01.2023**

# Inhaltsverzeichnis

- [FS-Konfigurationsdokumentation](#fs-konfigurationsdokumentation)
- [Inhaltsverzeichnis](#inhaltsverzeichnis)
- [Introduktion](#introduktion)
- [1. Aufgabenstellung](#1-aufgabenstellung)
- [2. Netzwerk](#2-netzwerk)
- [3. Konfiguration VM](#3-konfiguration-vm)
- [4. Samba-Installation](#4-samba-installation)
  - [4.1 Installation](#41-installation)
  - [4.2 Konfiguration](#42-konfiguration)
- [5. Client](#5-client)
  - [5.1 Client konfigurieren](#51-client-konfigurieren)
  - [5.2 Funktionstest](#52-funktionstest)
- [6. Quellen \& Troubleshoot](#6-quellen--troubleshoot)

# Introduktion
  <br>
  <img src="../04_Reliquum/GitLabOpuz/0000.png" alt="" width="170"/>

  **Samba-Fileshare auf Linux** <br>
  Quick-Step-Documentation
  Windows & Linux

# 1. Aufgabenstellung

Die Aufgabe war, einen Fileshare-Server auf Linux so vorzubereiten, damit es mit einem Windows-Client verbunden werden kann und die Dateien auf dem Linux Server auf dem Windows-Client ersichtlich sind.

Die Benutzernamen für die Einstellungen waren:

  - Linux-Fileshare-Server
    - Hostname: **Opusfileshare**
    - Benutzername: **Opus**
    - Passwort: **1234**
  
  - Samba
    - Benutzername: **Opus**
    - Passwort: **1234**

  - Windows-Client
    - Hostname: -
    - Benutzername: **OpusClient**
    - Passwort: **1234**

# 2. Netzwerk

VMnet 4 wurde als NAT-Netzwerk verwendet. Die wichtigsten Netzwerkdaten sind im Bild unterhalb dieses Textes enthalten.
<img src="../04_Reliquum/GitLabOpuz/0Visio.png" alt="" width="400"/>

# 3. Konfiguration VM

- **1**. Netzwerk und Benutzernamen sowie Hostname für Linux bestimmen. <br>
  <img src="../04_Reliquum/GitLabOpuz/001.png" alt="" width="300"/><br>
  <img src="../04_Reliquum/GitLabOpuz/002.png" alt="" width="300"/><br>
<br>

- **2**. Die Verbindungen von OSI-Layer 1 bis 3 überprüfen (1. Linux, 2. Windows). <br>
  <img src="../04_Reliquum/GitLabOpuz/003.png" alt="" width="300"/><br>
  <img src="../04_Reliquum/GitLabOpuz/004.png" alt="" width="300"/><br>
<br>

# 4. Samba-Installation
## 4.1 Installation

- **1**. Samba auf dem Linux-Server installieren und den Datenpfad überprüfen mit:
        
      1. sudo apt install samba
      2. whereis samba
   
   Bei *whereis samba* muss dabei das herauskommen:
      
      samba: /usr/sbin/samba /usr/lib/x86_64-linux-gnu/samba /etc/samba /usr/libexec/samba /usr/share/samba /usr/share/man/man7.gz /usr/share/man/man8/samba.8.gz
   
   **Nachweis**: <br> <img src="../04_Reliquum/GitLabOpuz/005.png" alt="" width="600"/>
<br>

## 4.2 Konfiguration

- **1**. Ein Directory mit Datenpfad */home/opus/sambashare* erstellen:

      mkdir /home/opus/sambashare

- **2**. In der Konfigurationsdatei *smb.conf* den folgenden Abschnitt hinzufügen. Wir erlauben unseren erstellten Ordner (sambashare) die Berechtigungen (Nur lesen und suchbar) und den Datenpfad.

      Öffnen mit -> sudo nano /etc/samba/smb.conf

      [sambashare]
          comment = Samba on Ubuntu
          Path = /home/opus/sambashare
          read only = no
          browsable = yes
  **Nachweis**: <br><img src="../04_Reliquum/GitLabOpuz/007.png" alt="" width="400"/>
<br>

- **3**. 1. Den SMBD-Service zur Aktualisierung neustarten. 2. Firewall abschalten, damit Samba mit dem Client kommunizieren kann.

      1. sudo systemctl restart smbd
      2. sudo ufw allow samba

- **4**. Da der Samba-User-Account (Opus) nicht den Passwort des Systems benutzt, müssen wir einen separat setzen:

      sudo smbpasswd -a opus
  **Nachweis**: <br><img src="../04_Reliquum/GitLabOpuz/009.png" alt="" width="400"/>
<br>

- **5**. In der Konfigurationsdatei den Netzwerkadapter mit Netzwerk-ID angeben, damit der Server weiss, über was der Server mit dem Client sprechen kann. WORKGROUP ebenfalls nach Belieben anpassen.
  
      Öffnen mit -> sudo nano /etc/samba/smb.conf

      [global]

      ## Browsing/Identification ###

      # Change this to the workgroup/NT-domain name your Samba Server will partake...
          workgroup = OPUSWORK (oder irgendetwas, was mit dem Computer übereinstimmt)

      # server string is the equivalent of the NT Description field
          server string = h% server (Samba, Ubuntu)

      #### Networking ####

      # The specific set of interfaces / network to bind to
      # This can be either the interface name or an IP address/netmask;
      # interface names are normally preferred
      ;  interfaces = 192.168.205.0/24 ens33 (IP-Adresse und Netzwerkadapter)
  **Nachweis**: <br><img src="../04_Reliquum/GitLabOpuz/011.png" alt="" width="600"/>
<br>

# 5. Client
## 5.1 Client konfigurieren
- **1**. Im **Control Panel** unter **Programs and Features** den **Windows-Features Box** öffnen. Runterscrollen und die folgende Option (**SMB 1.0/CIFS File Sharing Support**) anticken:
<br><img src="../04_Reliquum/GitLabOpuz/012.png" alt="" width="600"/>
<br>

## 5.2 Funktionstest
- **1**. Im Datei-Explorer unter Netzwerk die IP-Adresse des Servers eingeben, das folgende Resultat soll dargestellt werden:
<br><img src="../04_Reliquum/GitLabOpuz/013.png" alt="" width="300"/>
<br>

- **2**. Im sambashare-Ordner habe ich folgendes Ordner (**opussecrets**) erstellt:
<br><img src="../04_Reliquum/GitLabOpuz/014.png" alt="" width="600"/>
<br>

- **3**. Das gleiche kann man auf dem Linux-Server einsehen:
<br><img src="../04_Reliquum/GitLabOpuz/Zusatz.png" alt="" width="500"/>
<br>

- **4**. Für den Funktionstest habe ich eine Textdatei (**OPUSO.txt**) mit einem Wort erstellt:
<br><img src="../04_Reliquum/GitLabOpuz/016.png" alt="" width="600"/>
<br>

- **5**. Die Datei auf dem Linux-Server aufrufen oder in einer Liste wiedergeben:
<br><img src="../04_Reliquum/GitLabOpuz/017.png" alt="" width="600"/>
<br>

- **6**. Die Datei auf dem Linux-Server öffnen:
<br><img src="../04_Reliquum/GitLabOpuz/020.png" alt="" width="600"/>
<br>

# 6. Quellen & Troubleshoot

1. **[⧪ Samba-Fileshare Ubuntu 12.04 LTS](https://ubuntu.com/tutorials/install-and-configure-samba#3-setting-up-samba)**

2. **[⧪ Samba Connection Issues](https://learn.microsoft.com/en-us/answers/questions/977546/cant-connect-to-samba-share-(wins-10))**
   
3. **[⧪ Storage Troubleshoot](https://learn.microsoft.com/en-us/windows-server/storage/file-server/troubleshoot/detect-enable-and-disable-smbv1-v2-v3?tabs=server)**