# Quick-Step-Doc

Author: **Hoang Thang Nguyen** <br>
Updated Date: **24.01.2023**

# Inhaltsverzeichnis

- [Quick-Step-Doc](#quick-step-doc)
- [Inhaltsverzeichnis](#inhaltsverzeichnis)
- [Introduktion](#introduktion)
- [1. Aufgabenstellung](#1-aufgabenstellung)
- [2. Grobanleitung](#2-grobanleitung)

# Introduktion
  <br>
  <img src="../04_Reliquum/GitLabFilius/Logo.png" alt="" width="170"/>

  **Repetition von DHCP, DNS und sonstigen Dienste sowie Automatic Routing** <br>
  Quick-Step-Documentation
  Windows 

# 1. Aufgabenstellung

Die "Splendid Blur AG" verlangt die Einrichtung eines Netzwerks mit fünf Teilnetzwerken, die an einem Switch und folglich mit einem Router angebunden sind. Im Netzwerk ist ein Serverraum mit DNS, DHCP und Webserver vorhanden.

Die Aufgaben:
- Verbindungen erstellen und prüfen
- Mit dem ping-pong-Prinzip den Netzwerkverkehr prüfen
- Routing ermöglichen
- DHCP-Dienst aktivieren
- DNS-Dienst aktivieren
- Webserver aktivieren

<br><img src="../04_Reliquum/GitLabFilius/Übersicht.png" alt="" width="600"/>

# 2. Grobanleitung

- **1.** Die Netzwerkadapter des Routers konfigurieren, damit es später nicht vergessen geht.
<br><img src="../04_Reliquum/GitLabFilius/1.png" alt="" width="400"/>

- **2.** Die ganze Umgebung verbinden und die IP-Adressen, falls statisch, vergeben.
<br><img src="../04_Reliquum/GitLabFilius/2.png" alt="" width="400"/>

- **3.** Jedem Client eine IP-Adresse geben, falls statisch. Sonst DHCP-Option auswählen
<br><img src="../04_Reliquum/GitLabFilius/3.png" alt="" width="400"/>

- **4.** DHCP-Pool einrichten und die Angaben über den DNS-Server und Gateway (Router) eingeben.
<br><img src="../04_Reliquum/GitLabFilius/9.png" alt="" width="400"/>

- **5.** Auf einem Gerät den Command Prompt aktivieren, um Ping-Tests durchzuführen.
<br><img src="../04_Reliquum/GitLabFilius/5.png" alt="" width="400"/>

- **6.** IP-Adresse prüfen.
<br><img src="../04_Reliquum/GitLabFilius/6.png" alt="" width="400"/>

- **7.** A-Records gemäss den Vorgabe einrichten. Die Server kommen ebenfalls hinzu.
<br><img src="../04_Reliquum/GitLabFilius/7.png" alt="" width="400"/>

- **8.** Automatic-Routing anschalten.
<br><img src="../04_Reliquum/GitLabFilius/8.png" alt="" width="400"/>

- **9.** Unbedingt DNS und Gateway eintragen.
<br><img src="../04_Reliquum/GitLabFilius/10.png" alt="" width="400"/>

- **10.** Von verschiedenen Geräten aus Ping-Tests durchführen.
<br><img src="../04_Reliquum/GitLabFilius/12.png" alt="" width="400"/>
<br><img src="../04_Reliquum/GitLabFilius/13.png" alt="" width="400"/>
<br><img src="../04_Reliquum/GitLabFilius/14.png" alt="" width="400"/>

- **11.** DNS unbedingt anschalten, falls noch nicht getan.
<br><img src="../04_Reliquum/GitLabFilius/15.png" alt="" width="400"/>