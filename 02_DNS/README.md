# DNS-Konfigurationsdokumentation

Author: **Hoang Thang Nguyen** <br>
Updated Date: **12.01.2023**

# Inhaltsverzeichnis

- [DNS-Konfigurationsdokumentation](#dns-konfigurationsdokumentation)
- [Inhaltsverzeichnis](#inhaltsverzeichnis)
- [Introduktion](#introduktion)
- [1. Aufgabenstellung](#1-aufgabenstellung)
- [2. Netzwerkadaptervergabe](#2-netzwerkadaptervergabe)
  - [2.1 IP-Adressenvergabe](#21-ip-adressenvergabe)
- [3. Vorkonfiguration mit Windows](#3-vorkonfiguration-mit-windows)
- [4. DNS-Konfiguration](#4-dns-konfiguration)
  - [4.1 Zonen](#41-zonen)
  - [4.2 Records](#42-records)
- [5. Clientteil](#5-clientteil)
- [6. Linux](#6-linux)
  - [6.1 Linux Vorkonfiguration und erste Schritte mit DNS](#61-linux-vorkonfiguration-und-erste-schritte-mit-dns)
  - [6.2 Verbindungsversuch und Erklärung Netzwerkfehler](#62-verbindungsversuch-und-erklärung-netzwerkfehler)

# Introduktion
  <br>
  <img src="../04_Reliquum/GitLabAputh/00.png" alt="" width="170"/>

  **Domain Name System** <br>
  Konfigurationsdokumentation
  Windows & Linux

# 1. Aufgabenstellung

  Unsere Aufgabe ist es, einen DNS-Server im Windows-Server Image in einer virtuellen Maschine aufzusetzen und diese mit einem Client in Verbindung zu setzen und mit der Ping-Pong Methode und **nslookup** die Funktionsweise auszuprobieren und zu testen.
  Als kleines Extra wird das Ganze nochmals in Linux durchgespielt. Sollte aber keine grosse Herausforderung stellen.

  Die Schritte werden aufgelistet und beschrieben.

# 2. Netzwerkadaptervergabe

  Zuerst setzte ich ein kleines Netzwerk zwischen dem Client auf und setzte bei beiden eine statische IP-Adresse, damit sie immer erreichbar sind. Das war kein grosses Drama. Die Netz-ID war 192.168.245.0 und war auf NAT gesetzt. Der Client kann sich mit dem Internet verbinden, weiss aber nicht was zu tun ist, wenn er die IP-Adresse einer Webseite möchte. Demnach habe ich dem Client die IP-Adresse des DNS-Servers gegeben, damit er ihn befragen kann. Der DNS-Server hat dns.google (ein weiterer DNS-Server, welches unser DNS-Server per Rekursiv-Anfrage befragen kann, wenn er nicht weiss, was die IP-Adresse der Webseite ist) als Forwarder.

  ## 2.1 IP-Adressenvergabe

  *Windows* und *Ubuntu* DNS-Server: **192.168.245.130**
  Client: **192.168.245.150**
  <br>
  <img src="../04_Reliquum/GitLabAputh/01.png" alt="" width="400"/>

# 3. Vorkonfiguration mit Windows

  Kommen wir schon indirekt zur Sache. Da ich schon mehrmals eine VM aufsetzen musste, verzichte ich diesmal auf eine detaillierte Dokumentation über die Konfiguration einer VM. Unterhalb dieses Texts sieht man ein Bild, wie ich die ISO-Image eines Windows-Servers in den VM-Einstellungen eingebe.<br>
  <img src="../04_Reliquum/GitLabAputh/02.png" alt="" width="400"/>
  <br>

  Damit man die Terminalversion des Servers erspart, sollte man auf die Desktop Experience wechseln, sodass wir einen schönen GUI haben. Ist sehr empfohlen, kann man aber auch nur mit dem Terminal machen, wenn man besonders viel Herausforderung haben will. Das erzeugt zu viel Aufwand, deshalb habe ich das ausgelassen.<br>
  <img src="../04_Reliquum/GitLabAputh/03.png" alt="" width="400"/>
  <br>

  Für uns **advanced** Informatiker sollten wir die Custom-Option auswählen. Damit wird nur die OS laden ohne Extrafunktionen.<br>
  <img src="../04_Reliquum/GitLabAputh/04.png" alt="" width="400"/>
  <br>

  Nachdem weitere Konfigurationen gemacht wurden, sollte das GUI im Bild aufpoppen und der Server Manager unsere Gesichter zum Leuchten bringen.<br>
  <img src="../04_Reliquum/GitLabAputh/05.png" alt="" width="400"/>
  <br>

  Nächstes werden die VM-Tools heruntergeladen, damit der Bildschirm etwas grösser ist. Das ist ein Standard, wenn man irgendwelche virtuellen Maschinen mit Windows OS aufsetzt.<br>
  <img src="../04_Reliquum/GitLabAputh/06.png" alt="" width="400"/>
  <br>

# 4. DNS-Konfiguration

  Damit ich die DNS-Funktionen herunterladen kann, muss ich erstmal auf den Server Manager gehen und die Features installieren. Der Server-Manager verwaltet die Funktionen und ist der Kern der Verwaltung des Servers und seine Funktionen. Es können mehrere Serverrollen auf dem Windows-Server laufen. Damit kann unser Server ein Multitasking-Server sein und mehrere Aufgaben beherbergen. 

  Z.B. Domain-Controller + Active-Directory + DNS/DHCP.<br>
  <img src="../04_Reliquum/GitLabAputh/07.png" alt="" width="500"/>
  <br>

  Mit dieser Maske wählt man die Rolle des Servers und lädt man sie runter. Hier kann man ebenfalls die benötigten Tools der Serverrolle herunterladen. Diese Maske unterscheidet sich mit dem von Linux. Bei Windows bekommt man ein schönes GUI für die Installation für die Serverrollen und dessen Tools. Bei Linux muss man leider alles manuell mit Befehlen via dem Terminal machen, damit man einen Server konfiguriert hat. Ein grosses Plus für Windows-Server, der ist aber nicht so flexibel wie Linux.<br>
  <img src="../04_Reliquum/GitLabAputh/08.png" alt="" width="500"/>
  <br>

  Ich wähle mein Server aus, der hat noch keine IP-Adresse, weil ich ihm noch keine gegeben habe. Damit er sich mit dem Client unterhalten kann, werde ich es noch unverzüglich tun. Das kann man normal in den Systemsteuerung unter Netzwerk und Internet machen. Es werden aber keine Details dazu genannt.<br>
  <img src="../04_Reliquum/GitLabAputh/09.png" alt="" width="500"/>
  <br>

  Jetzt wähle ich meine gewollten Features und Rollen aus, welches der Server dann übernehmen wird. Aus dem Server wird ein DNS-Server. Im ersten Bild werden die Tools für den DNS ausgewählt. Im zweiten Bild wird die Rolle entsprechend ausgewählt.<br> 
  <img src="../04_Reliquum/GitLabAputh/010.png" alt="" width="500"/><br>
  <img src="../04_Reliquum/GitLabAputh/011.png" alt="" width="500"/>
  <br>

  Dann wird unser DNS-Server installiert, es folgt ein Neustart, damit alle Tools korrekt geladen werden. Somit haben wir das Herunterladen vervollständigt. Demnächst machen wir die einfache Konfiguration des DNS-Servers mit den Zonen und seinen A-Records.<br>
  <img src="../04_Reliquum/GitLabAputh/012.png" alt="" width="300"/>
  <br>

## 4.1 Zonen

  Es gibt mehrere Zonen in einem DNS-Server, ich werde aber nur zwei einrichten, nämlich den Forward- und Reverse-Lookup-Zone. Ein Forward-Lookup ist, wenn der DNS-Server eine herkömmliche Webseiten-Domäne in eine IP-Adresse umwandelt. Ein Reverse-Lookup ist das genau Umgekehrte. In den Zonen definiere ich, was in was umgewandelt wird. Wir versuchen es mit dem Hostname und IP-Adresse des Clients sowohl auch des DNS-Servers und versuchen, diese mit dem Hostname anzupingen.

  Ich gehe noch weiter und füge einen Forwarder hinzu, damit wenn der DNS-Server keine Ahnung hat, wie er eine Webseite auflösen soll, einen anderen DNS-Server im Internet via Rekursiv-Anfrage anfragen kann. Z.B. Google-DNS wird antworten und die Lösung dem lokalen DNS-Server zu schicken, damit wiederum der Lokal-DNS dem Client, die IP-Adresse der Webseite zu schicken kann. Ich versuche es mit 20min.ch.

  Gehen wir den Schritten nach:

  Im Server Manager unter Tools finden wir den Eingang in der DNS-Applikation zur Verwaltung des DNS und seinen Zonen.<br>
  <img src="../04_Reliquum/GitLabAputh/013.png" alt="" width="300"/>
  <br>

  Ich erstelle jetzt eine neue Zone, wo alle Records und sonstige Angaben zur Namensauflösung gespeichert werden. Der DNS benutzt diese als Ablageort für seine Records, sonst kann er keine Angaben machen, somit ist unser DNS nutzlos.<br>
  <img src="../04_Reliquum/GitLabAputh/014.png" alt="" width="300"/>
  <br>

  Ich erstelle eine primäre Zone für den DNS, es ist für unseren Fall praktischer, da sie weniger Konfigurations- und Aufwandsprobleme aufzeigt. Die primäre Zone liegt auf unserem Server. Die sekundäre Zone befindet sich auf einem anderen Server, falls vorhanden. Der Stub Zone ist eine Kopie eines der beiden Zonen und dient nicht als autoritativ (also nicht verantwortlich).<br>
  <img src="../04_Reliquum/GitLabAputh/015.png" alt="" width="300"/>
  <br>

  Ich beginne mit der Forward-Lookup-Zone, wo die Namen zu IP-Adressen werden. Dort sind alle Angaben, die die Namensauflösung ermöglichen, gespeichert.<br>
  <img src="../04_Reliquum/GitLabAputh/016.png" alt="" width="300"/>
  <br>

  Ich gebe jetzt einen schönen ausgedachten Namen für die Zonendatei (Speicherort als Datei) ein und definiere den Namen der Zone. Damit ist unsere Forward-Lookup-Zone fertig.<br>
  <img src="../04_Reliquum/GitLabAputh/017.png" alt="" width="300"/>
  <br>

  Das Gleiche mit der Reverse-Lookup-Zone, einfach nur jetzt, dass nicht ein Name eingegeben wird, sondern eine IP-Adresse, da ein Reverse-Lookup eine IP-Adresse in einen Namen auflöst.<br> 
  <img src="../04_Reliquum/GitLabAputh/018.png" alt="" width="300"/>
  <br>

  Jetzt gebe ich der Zone die Netz-ID ein.<br>
  <img src="../04_Reliquum/GitLabAputh/019.png" alt="" width="300"/>
  <br>

## 4.2 Records

  Damit unser DNS-Server ein bisschen Leben spürt, geben wir ihm ein paar definierte Records, sodass er sie Anfragen für die Namensauflösung auflösen kann. Ganz simpel.
  Auf dem erstellten Forward-Lookup-Zones erstellen wir einen A-Record (ein Dualwertepaar von Namen und IP-Adresse). Die A-Records sind die Speichereinheiten des DNS-Servers, wo die Wertepaaren von IP-Adresse und Hostname gespeichert werden.<br>
  <img src="../04_Reliquum/GitLabAputh/020.png" alt="" width="300"/>
  <br>

  Ich gebe dem A-Record irgendetwas rein, das mit der Netz-ID übereinstimmt. Mit PTR-Record wird in der Reverse-Lookup-Zone ein gleichwertiger Record eingetragen, nur andersrum diesmal.<br>
  <img src="../04_Reliquum/GitLabAputh/021.png" alt="" width="300"/>
  <br> 

  Zwei A-Records habe ich eingetragen, damit unser Server und unser Client mit den Adressen aufgelöst werden können. So sieht es aus in der Forward-Lookup-Zone.<br>
  <img src="../04_Reliquum/GitLabAputh/022.png" alt="" width="300"/>
  <br> 

  Dank dem PTR-Record Pointer im ersten Bild, wird automatisch in der Reverse-Lookup-Zone ein PTR-Record erstellt, es ist das Antonym gegenüber dem A-Record.<br>
  <img src="../04_Reliquum/GitLabAputh/023.png" alt="" width="300"/>
  <br> 

  Mit einem Rechtsklick auf dem Servernamen und dann unter Eigenschaften gehen, kann man den Forwarder einrichten. Dort werden die Anfragen für den DNS-Server weitergeleitet, wenn dieser keine Informationen austeilen kann. Der Forwarder ist ein Server meistens ausserhalb des Netzwerkes, dns.google ist meistens vertreten. Im Bild auf der nächsten Seite sieht man das Fenster zum Hinzufügen.<br>
  <img src="../04_Reliquum/GitLabAputh/024.png" alt="" width="300"/>
  <br> 

# 5. Clientteil

  Wir kommen jetzt zum Testen der Umgebung. Ich gebe dem Server die IP-Adresse = **192.168.245.130** und dem Client die IP-Adresse = **192.168.245.150**.

  **Vorausgesetzt**: Der Client ist vollständig nach Belieben eingerichtet. Es werden keine Details dazu genannt.

  Die Command Prompt des Windows Clients als Administrator ausführen und nslookup eingeben und exekutieren. Es scheint nicht zu funktionieren, da möglicherweise die IP-Adresse noch nicht richtig aufgenommen wurde. Mit ein paar Kontrollen in den Netzwerkeinstellungen und Netzwerkkarteneinstellungen wird es sicher funktionieren. Falls notwendig, Adapter wechseln, IP-Adresse neu ausdenken und einfügen. Der DNS-Server muss zudem die Records für sich aufnehmen. Das kann ein bisschen Zeit brauchen.<br> 
  <img src="../04_Reliquum/GitLabAputh/025.png" alt="" width="300"/>
  <br> 

  Ein paar Minuten später soll der DNS eingerichtet sein. Ich versuche es nochmals mit nslookup. Es zeigt mir an, welcher DNS-Server mit meinem Client verbunden ist. In diesem Fall ist unser eingerichteter DNS-Server verbunden und es hat funktioniert.<br>
  <img src="../04_Reliquum/GitLabAputh/026.png" alt="" width="300"/>
  <br> 

  Im Bild unten versuche ich nochmals mit nslookup meinen DNS-Server zu orten. **Erfolgreich**. Ich versuche die IP-Adresse des DNS-Servers aufzulösen, es gibt mir seinen Namen zurück. Ich versuche als nächstes den Namen aufzulösen, es gibt mir seinen Namen und IP-Adresse zurück.<br>
  <img src="../04_Reliquum/GitLabAputh/027.png" alt="" width="300"/>
  <br> 

  Ich versuche jetzt 20 Minuten aufzulösen, das wird sicherlich nicht mit meinen lokalen DNS-Server geschehen, sondern mit google.dns. Es zeigt mir an, dass das ein nicht aus dem lokalen DNS-Server gekommenen Antwort ist, sondern von einem anderen, nämlich unserem Forwarder (dns.google). Der lokaler DNS-Server ist für diese Antwort nicht verantwortlich.<br>
  <img src="../04_Reliquum/GitLabAputh/028.png" alt="" width="300"/>
  <br> 

  Die ganz klassische Art mit ping auf der Webseite, ohne dem DNS-Server ist das nicht mal möglich. Man sieht ausserdem, dass ich 20min.ch, also eine Domäne eingegeben habe, das wäre ohne dem DNS-Server gar nicht möglich.<br>
  <img src="../04_Reliquum/GitLabAputh/029.png" alt="" width="300"/>
  <br> 

  Weitere klassische Pings mit dns.google:<br>
  <img src="../04_Reliquum/GitLabAputh/030.png" alt="" width="300"/>
  <br> 

  # 6. Linux

  Der Windows-Client bleibt bestehen, damit kein Zusatzaufwand erzeugt wird. Es kommt nicht darauf an, wie der Client aufgestellt ist. Es soll nur die DNS-Adresse des Linux DNS-Servers erhalten, damit die ganzen Prozesse laufen.
  Als Zusatzherausforderung mache ich einen Linux-Server und verbinde diese mit dem Client. Auf Linux laufen einige Dinge ganz anders. Diese werde ich unterhalb diesem Text vermerken.
  Die **IP-Adressen** lauten:
  -	Client -> **192.168.245.150**
  -	Linux DNS-Server -> **192.168.245.130 + NAT: 192.168.200.128**
<br>
  <img src="../04_Reliquum/GitLabAputh/031.png" alt="" width="500"/>
  <br> 

  ## 6.1 Linux Vorkonfiguration und erste Schritte mit DNS


  **1**.	Wie beim Windows-DNS-Server fange ich mit den Einstellungen für die virtuelle Maschine an. Das ist keine wilde Sache. Ich gebe jede Komponente genügend Leistung, damit die VM nicht plötzlich abstürzt oder Mangel aufzeigt.<br>
  <img src="../04_Reliquum/GitLabAputh/032.png" alt="" width="400"/> <br>

  **2**.  Da Linux nicht gerne zwei Interfaces sieht, habe ich die erste NAT-Verbindung, also der erste Netzwerkadapter als NAT gesetzt, damit er über diese eine stabile Verbindung mit dem Internet hat und die Datenpakete von dort abholen kann. Mit einem kombinierten Netzwerk von zwei Interfaces funktioniert der Linux-Server nicht so wie erwartet, deswegen fange ich mit dem Internetteil an.<br>
  <img src="../04_Reliquum/GitLabAputh/033.png" alt="" width="400"/> <br>

  **3**. Nach dem Ausfüllen von diversen anderen Sachen wie Benutzername und Hostname für den DNS-Server sowie Festplattenpartitionen habe ich die Installation eingeleitet, die dauert meistens 10-15 Minuten. Im Bild sind verschiedene Prozesse für die Installation zu sehen.<br>
  <img src="../04_Reliquum/GitLabAputh/034.png" alt="" width="400"/><br>

  **4**.	Nachdem Einloggen in den Server habe ich sofort diesen Befehl durchgeführt, damit der Server Updates beim Hauptserver für Linux-Programme abholt.<br>
      ```sudo apt-get update```

  **5**.	Diese Updates werden mit dem Befehl vom Bild jetzt heruntergeladen und auf dem Server gebracht.<br>
    ```sudo apt-get upgrade```  

  **6**.	Ich lade das ganze System für DNS-Linux herunter, Bind ist ein bekanntes Tool für das Betreiben eines DNS-Servers auf Linux.<br>
    ```sudo apt-get install bind9```

  **7**.	Dnsutils ist für Kontrollchecks und Patches bekannt, damit kontrolliert man Konfigurationen und Verbindungen. Muss nicht unbedingt heruntergeladen werden.<br>
    ```sudo apt-get install dnsutils``` 

  **8**.	Mit dem Befehl oberhalb des Bildes gelingt man in die Hauptkonfiguration des DNS-Servers. Dort füge ich einen Forwarder hinzu. 8.8.8.8 = google.dns
  Die // muss man bei den drei Zeilen entfernen, damit sie nicht auskommentiert werden. Nachdem sollte man den Service mit ``sudo systemctl restart bind9`` neustarten.<br>
    ```sudo nano /etc/bind/named.conf.options```<br> 
  <img src="../04_Reliquum/GitLabAputh/035.png" alt="" width="400"/><br> 

  **9**.	Mit dem dig-Befehl überprüft man eine Verbindung oder stellt eine her. In diesem Fall habe ich mit google.com versucht eine Verbindung herzustellen, mit Erfolg.<br>
  <img src="../04_Reliquum/GitLabAputh/036.png" alt="" width="400"/><br> 

  **10**.	Die folgenden include-Teile sollten nicht auskommentiert sein, damit sie Erwähnungen und Konfigurationsdateien anzeigen kann.<br>  
  <img src="../04_Reliquum/GitLabAputh/037.png" alt="" width="400"/><br> 

  **11**.	Wir fügen bei der Zonenkonfiguration named.conf.local eine Zone hinzu, für dieses Mal eine **Forward-Lookup-Zone**. Vorher sollte man die Datei noch kopieren bevor man sie verlinkt.<br>
    ``sudo cp /etc/bind/db.local /etc/bind/db.example.com``
    ``sudo nano /etc/bind/named.conf.local``
  <img src="../04_Reliquum/GitLabAputh/037.png" alt="" width="400"/><br>

  **12**.	Man öffnet die Datei im Bild oben mit ``sudo nano /etc/bind/db.example.com``. Man sollte ausserdem die Stellungen der einzelnen Werten beachten. Ich habe oben die IP-Adresse später wieder zurückgeschoben.<br>
  <img src="../04_Reliquum/GitLabAputh/038.png" alt="" width="400"/><br> 

  **13**.	Jetzt erstellen wir eine Reverse-Lookup-Zone. Die Zonenname beinhaltet die drei ersten Oktette unseres Netzwerk-IDs. Es wird auch auf einer weiteren Konfigurationszonendatei gezeigt. Zuerst sollte man noch die einte Datei kopieren und zu unseren Zonendatei mit dem ersten Oktett unseres Netzwerks erstellen.<br>
    ``sudo cp /etc/bind/127 /etc/bind/db.192``
    ``sudo nano /etc/bind/named.conf.local``
  <img src="../04_Reliquum/GitLabAputh/039.png" alt="" width="400"/><br> 

  **14**.	Die Konfigurationen habe ich so weit bearbeitet, damit es meinem Netzwerk bestimmt ist. Wenn schon nicht vorher erwähnt, ist unsere Domäne *example.com*.<br>
  <img src="../04_Reliquum/GitLabAputh/040.png" alt="" width="400"/><br>

  ## 6.2 Verbindungsversuch und Erklärung Netzwerkfehler

  **1**.  Jetzt ist es an der Zeit, den zweiten Netzwerkadapter anzuschliessen und die Verbindung zwischen Client und dem DNS-Server zu ermöglichen. Es wird ein privates Netzwerk gebildet.<br>
  <img src="../04_Reliquum/GitLabAputh/041.png" alt="" width="400"/><br>

  **2**.	Da der zweite Adapter noch nicht konfiguriert wurde (erstes Bild auf der Seite), kann sich der DNS-Server daher auch nicht mit dem Client verbinden. Mit sudo nano /etc/netplan/00-installer-config.yaml kommt man zur Konfigurationsdatei mit den Adaptern. Im zweiten Bild habe ich die Konfigurationen und Adressen definiert. So kann der DNS-Server mit dem Client sprechen, mit dem Internet aber seit Anfang aber nicht, da Linux eine spezielle Konfiguration braucht, die ich leider bis jetzt noch nicht herausgefunden habe.<br>
  <img src="../04_Reliquum/GitLabAputh/042.png" alt="" width="400"/><br>
  <img src="../04_Reliquum/GitLabAputh/043.png" alt="" width="400"/><br>

  Jetzt hat der zweite Netzwerkadapter (Host-Only) eine IP-Adresse.<br>
  <img src="../04_Reliquum/GitLabAputh/044.png" alt="" width="400"/><br>  

  **3**.	Die Zeit für ein Test ist gekommen. Ich versuche jetzt OPUSCLIENT, also der Client anzupingen. Das hat nach dem Bild nach super funktioniert.<br>
  <img src="../04_Reliquum/GitLabAputh/045.png" alt="" width="400"/><br>
  <img src="../04_Reliquum/GitLabAputh/046.png" alt="" width="400"/><br>

  **4**.	Auf dem CMD des Clients versuchen wir den DNS-Server anzupingen, mit Erfolg! Somit hat die Konfiguration funktioniert. Die Internetverbindung hat leider bis jetzt nicht so funktioniert, wie erwartet. Man muss ein Postrouting durchführen, bisher habe ich es noch nie versucht, deswegen bleiben wir auf lokaler Netzwerk-Ebene.<br>
  <img src="../04_Reliquum/GitLabAputh/047.png" alt="" width="400"/><br>