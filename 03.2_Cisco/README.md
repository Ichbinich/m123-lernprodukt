# Quick-Step-Doc

Author: **Hoang Thang Nguyen** <br>
Updated Date: **17.01.2023**

# Inhaltsverzeichnis

- [Quick-Step-Doc](#quick-step-doc)
- [Inhaltsverzeichnis](#inhaltsverzeichnis)
- [Introduktion](#introduktion)
- [1. Aufgabenstellung](#1-aufgabenstellung)
- [2. Konfiguration des Gesamtnetzwerks](#2-konfiguration-des-gesamtnetzwerks)

# Introduktion
  <br>
  <img src="../04_Reliquum/GitLabCischko/Logo.png" alt="" width="170"/>

  **Netzwerk-Installations- und Konfigurationsanleitung für die Firma Clearwear AG** <br>
  Quick-Step-Documentation
  Windows 

# 1. Aufgabenstellung

Wir haben von der Firma „Clearwear AG.“ den Auftrag erhalten, die Netzwerkdienste 
DHCP und DNS einzurichten.
Folgende Aufgaben sind zu erfüllen
- „Quick-Step-Doc erstellen mit dem Namen „Netzwerk-Installations- und 
Konfigurationsanleitung für die Firma „Clearwear AG.“
- Vorbereitetes CISCO-Packet Tracer-Template runterladen und öffnen
- Netzwerk und Dienste gem. Netzwerkplan unten einrichten
- Funktionalität überprüfen / allenfalls Troubleshooting
- Vorgaben:
  - Kein Gateway definieren und eintragen (da kein Router vorhanden)
  - Namenskonvention beachten: PC L-111 erhält IP 192.168.0.111 /24
  - DNS A-Records nur für Geräte mit fixen IP-Adressen erstellen
  - DNS A-Records gem. Netzwerkplan unten (z.B. dhcp-server oder L-111)
  - DHCP-Pool: 192.168.0.150 – 192.168.0.200
  - Ablauf in der oben erstellten Doku festhalten (Beweis mit eigenen Screenshots)
- Persönliche Überlegungen, Veränderungs- und Verbesserungsvorschläge
(welche Änderungen wären aus Ihrer Sicht noch sinnvoll und nützlich?)

(Author: TBZ, Modul 123 – Hands-on Challenge -
„Serverdienste DHCP und DNS für ein KMU einrichten)

# 2. Konfiguration des Gesamtnetzwerks

- **1.** Das ganze Netzwerk verbinden (Layer 1), dafür benutzt man das Kabeltool mit dem Blitz. Man sollte jedes Gerät anschalten, sonst funktioniert auch nichts.
<br><img src="../04_Reliquum/GitLabCischko/0.png" alt="" width="400"/>

- **2.** DHCP-Server mit der vorgegebenen IP-Adresse versehen.
<br><img src="../04_Reliquum/GitLabCischko/1.png" alt="" width="400"/>

- **3.** Jedes Clientgerät mit statischer IP-Adressenvergabe konfigurieren.
<br><img src="../04_Reliquum/GitLabCischko/2.png" alt="" width="400"/>

- **4.** Jeder Client soll die IP-Adresse des DNS haben.
<br><img src="../04_Reliquum/GitLabCischko/3.png" alt="" width="400"/>

- **5.** DNS-Server mit IP-Adresse versehen.
<br><img src="../04_Reliquum/GitLabCischko/4.png" alt="" width="400"/>

- **6.** Verbindungstests auf den Clients durchführen.
<br><img src="../04_Reliquum/GitLabCischko/5.png" alt="" width="400"/>

- **7.** DHCP-Pool konfigurieren.
<br><img src="../04_Reliquum/GitLabCischko/6.png" alt="" width="400"/>

- **8.** DHCP-Clients konfigurieren, dafür auf **DHCP** stat **statisch**.
<br><img src="../04_Reliquum/GitLabCischko/7.png" alt="" width="400"/>

- **9.** Auf den Clients die IP-Adresse releasen und einen neuen holen.
<br><img src="../04_Reliquum/GitLabCischko/8.png" alt="" width="400"/>

- **10.** Auf dem DNS-Server A-Records eintragen.
<br><img src="../04_Reliquum/GitLabCischko/9.png" alt="" width="400"/>

- **11.** Das ganze Netzwerk funktioniert.
<br><img src="../04_Reliquum/GitLabCischko/10.png" alt="" width="400"/>